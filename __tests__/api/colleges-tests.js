const request = require('supertest');
const jwt = require('jsonwebtoken');

const app = require('../../src/app');
const { prePopulate } = require('../../src/helper/prePopulate');
const db = require('../../src/helper/mongoose');

const College = require('../../src/api/colleges/colleges-model');
const Admin = require('../../src/api/admins/admins-model');

let id;
const admin = {
  username: 'college',
  hashword: '$2b$12$rX/NeqiQ6W8ulOLVKgdOo.1un7.Z.JbEuwWNHCDGWDCiXJeIbc.Qy', // Hashword for 'adminpassword'
  invalid_jwt: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDBkYjQyYmFlOGU2NDM0MzIyNjAxOGYiLCJpYXQiOjE1NjExNzkzNTR9.Q3yR5ktIYzSTZKYItjODBaMmVIx1fFm8VMQASeUCZd4',
};

const doc = { name: 'Populate College' };


beforeAll(async () => {
  await db.connect();
  await prePopulate(College, doc);
  id = await prePopulate(Admin, admin);
  // TODO: Seperate signing to a function
  admin.jwt = jwt.sign({ _id: id }, process.env.JWT_ADMIN_SECRET);
});

afterAll(async () => {
  // await College.deleteMany({});  // Need to test whether globalteardown is better than this
  // await Admin.deleteMany({});
  db.close();
});

// --- Start of Unit Tests ---

describe('GET /colleges', () => {
  it('Should respond with a 200 status code', () => request(app)
    .get('/colleges')
    .expect(200));
  it('Should respond with a colleges array', () => request(app)
    .get('/colleges')
    .expect(200)
    .then((res) => {
      expect(res.body).toEqual(
        expect.objectContaining({
          colleges: expect.any(Array),
        }),
      );
    }));
  it('Should rerespond with an list of colleges', () => request(app)
    .get('/colleges')
    .expect(200)
    .then((res) => {
      expect(res.body.colleges).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            _id: expect.stringMatching(/^[a-f0-9]{24}$/),
            name: expect.any(String),
          }),
        ]),
      );
    }));
});

describe('POST /colleges', () => {
  it('without body should respond with a 400 status code', () => request(app)
    .post('/colleges')
    .expect(400));
  it('without body should respond with an error message', () => request(app)
    .post('/colleges')
    .expect(400)
    .then((res) => {
      expect(res.body.status).toBe(400);
      expect(res.body.message).toBe('"name" is required.');
    }));
  it('without token should respond with a 401 status code', () => request(app)
    .post('/colleges')
    .send({ name: 'Test College' })
    .expect(401)
    .then((res) => {
      expect(res.body.message).toBe('No auth token');
    }));
  it('with valid token and body should respond with a success message', () => request(app)
    .post('/colleges')
    .send({ name: 'Test College' })
    .set('Authorization', `Bearer ${admin.jwt}`)
    .expect(200)
    .then((res) => {
      expect(res.body.message).toEqual(
        expect.stringMatching(/^New college added with id ([a-f0-9]{24})$/),
      );
    }));
  it('with valid token and duplicate body should respond with an error message', () => request(app)
    .post('/colleges')
    .send({ name: 'Test College' })
    .set('Authorization', `Bearer ${admin.jwt}`)
    .expect(400)
    .then((res) => {
      expect(res.body.status).toBe(400);
      expect(res.body.message).toEqual(
        expect.stringContaining('duplicate key error'),
      );
    }));
  // TODO Test afterall, clearing and stuff
});
