const request = require('supertest');

const app = require('../../src/app');
const { prePopulate } = require('../../src/helper/prePopulate');
const db = require('../../src/helper/mongoose');

const Admin = require('../../src/api/admins/admins-model');
const Participant = require('../../src/api/participants/participants-model');

const admin = {
  username: 'populate',
  hashword: '$2b$12$rX/NeqiQ6W8ulOLVKgdOo.1un7.Z.JbEuwWNHCDGWDCiXJeIbc.Qy', // Hashword for 'adminpassword'
};
const participant = {
  authClass: 'local',
  authID: 'populate',
  hashword: '$2b$12$MEsXvgafqayOGbXrzIt5Q.OPnivo0yx0RVeEBaGa5.EoBeTSRZswy', // Hashword for 'participant'
};


beforeAll(async () => {
  await db.connect();
  await Promise.all([prePopulate(Admin, admin), prePopulate(Participant, participant)]);
});

afterAll(async () => {
  // await Promise.all([Admin.deleteMany({}), Participant.deleteMany({})]);
  db.close();
});


describe('POST /auth/participant/register', () => {
  it('without body, should respond with error message', () => request(app)
    .post('/auth/participant/register')
    .expect(400)
    .then((res) => {
      expect(res.body.status).toBe(400);
      expect(res.body.message).toBe('"username" is required.');
    }));
  it('with empty username, should respond with error message', () => request(app)
    .post('/auth/participant/register')
    .send({ username: '', password: 'random' })
    .expect(400)
    .then((res) => {
      expect(res.body.status).toBe(400);
      expect(res.body.message).toBe('"username" is not allowed to be empty.');
    }));
  it('with empty password, should respond with error message', () => request(app)
    .post('/auth/participant/register')
    .send({ username: 'random', password: '' })
    .expect(400)
    .then((res) => {
      expect(res.body.status).toBe(400);
      expect(res.body.message).toBe('"password" is not allowed to be empty.');
    }));
  it('with duplicate username, should respond with error message', () => request(app)
    .post('/auth/participant/register')
    .send({ username: participant.authID, password: 'random' })
    .expect(400)
    .then((res) => {
      expect(res.body.status).toBe(400);
      expect(res.body.message).toEqual(
        expect.stringContaining('duplicate key error'),
      );
    }));
  it('with valid body, should respond with success message', () => request(app)
    .post('/auth/participant/register')
    .send({ username: 'testuser', password: 'random' })
    .expect(200)
    .then((res) => {
      expect(res.body.message).toEqual(
        expect.stringMatching(/^Participant with id ([a-f0-9]{24}) has been registered$/),
      );
    }));
});


describe('POST /auth/admin/register', () => {
  it('without body, should respond with error message', () => request(app)
    .post('/auth/admin/register')
    .expect(400)
    .then((res) => {
      expect(res.body.status).toBe(400);
      expect(res.body.message).toBe('"username" is required.');
    }));
  it('with empty username, should respond with error message', () => request(app)
    .post('/auth/admin/register')
    .send({ username: '', password: 'random' })
    .expect(400)
    .then((res) => {
      expect(res.body.status).toBe(400);
      expect(res.body.message).toBe('"username" is not allowed to be empty.');
    }));
  it('with empty password, should respond with error message', () => request(app)
    .post('/auth/admin/register')
    .send({ username: 'random', password: '' })
    .expect(400)
    .then((res) => {
      expect(res.body.status).toBe(400);
      expect(res.body.message).toBe('"password" is not allowed to be empty.');
    }));
  it('with duplicate username, should respond with error message', () => request(app)
    .post('/auth/admin/register')
    .send({ username: admin.username, password: 'random' })
    .expect(400)
    .then((res) => {
      expect(res.body.status).toBe(400);
      expect(res.body.message).toEqual(
        expect.stringContaining('duplicate key error'),
      );
    }));
  it('with valid body, should respond with success message', () => request(app)
    .post('/auth/admin/register')
    .send({ username: 'testuser', password: 'random' })
    .expect(200)
    .then((res) => {
      expect(res.body.message).toEqual(
        expect.stringMatching(/^Added admin with id: ([a-f0-9]{24})$/),
      );
    }));
});

describe('POST /auth/participant/get-token', () => {
  it('without body, should respond with 400 statuscode', () => request(app)
    .post('/auth/participant/get-token')
    .expect(400));
  it('with empty fields, should respond with 400 statuscode', () => request(app)
    .post('/auth/participant/get-token')
    .send({ username: '', password: '' })
    .expect(400));
  it('with nonexistant credentials should respond with 404 not found', () => request(app)
    .post('/auth/participant/get-token')
    .send({ username: 'nonexistant', password: 'random' })
    .expect(404));
  it('with invalid credentials should respond with 401 unauthorized', () => request(app)
    .post('/auth/participant/get-token')
    .send({ username: participant.authID, password: 'random' })
    .expect(401));
  it('with valid credentials, should respond with success message', () => request(app)
    .post('/auth/participant/get-token')
    .send({ username: participant.authID, password: 'participant' })
    .expect(200)
    .then((res) => {
      expect(res.body.username).toBe(participant.authID);
      expect(res.body.token).toBeDefined();
    }));
});

// TODO Tests for character length and other validation restrictions
describe('POST /auth/admin/get-token', () => {
  it('without body, should respond with 400 statuscode', () => request(app)
    .post('/auth/admin/get-token')
    .expect(400));
  it('with empty fields, should respond with 400 statuscode', () => request(app)
    .post('/auth/admin/get-token')
    .send({ username: '', password: '' })
    .expect(400));
  it('with nonexistant credentials should respond with 404 not found', () => request(app)
    .post('/auth/admin/get-token')
    .send({ username: 'nonexistant', password: 'random' })
    .expect(404));
  it('with invalid credentials should respond with 401 unauthorized', () => request(app)
    .post('/auth/admin/get-token')
    .send({ username: admin.username, password: 'random' })
    .expect(401));
  it('with valid credentials, should respond with success message', () => request(app)
    .post('/auth/admin/get-token')
    .send({ username: admin.username, password: 'adminpassword' })
    .expect(200)
    .then((res) => {
      expect(res.body.username).toBe(participant.authID);
      expect(res.body.token).toBeDefined();
    }));
});
