/*
//This file would connect mongoose to mongodb
//Connection should not be done anywhere else
//Require this file once in app.js and that will establish the connection
*/

const mongoose = require('mongoose');

const keys = require('../config/keys');

const { NODE_ENV } = keys;
const { MONGO_TEST_URI, MONGO_URI } = keys.mongodb;

// Fixing deprication warning
const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
};

function connect() {
  if (NODE_ENV === 'test') {
    return mongoose.connect(MONGO_TEST_URI, options);
  }
  return mongoose.connect(MONGO_URI, options)
    .then(() => {
      console.log('Connected to mongoose');
    })
    .catch((err) => {
      console.log('Couldnt connect to mongoose:', err.message);
      process.exit(1);
    });
}

function close() {
  return mongoose.connection.close();
}

module.exports = {
  connect,
  close,
};
