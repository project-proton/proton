const { instamojo } = require('../config/keys');

class Instamojo {
    /**
     * Returns {
     *  payment_request_id: string, 
     *  payment_request_url: string
     * }
     */
    createPaymentRequest(amount, expires_at) {
        const request = {
            method: 'post',
            url: `${instamojo.BASE_URL}/payment-requests/`,
            data: {
                amount: amount,
                purpose: `Cart: ${user.name}`,
                buyer_name: user.name,
                email: user.email,
                phone: user.mobile,
                redirect_url: instamojo.REDIRECT_URL,
                webhook: instamojo.WEBHOOK_URL,
                allow_repeated_payments: 'false',
                send_email: 'true',
                send_sms: 'true',
                expires_at: expires_at,
            },
            headers: {
                'x-api-key': instamojo.API_KEY,
                'x-auth-token': instamojo.AUTH_TOKEN,
                'content-type': 'application/json',
            },
        };
        const response = await axios(request);
        let { payment_request: { id, longurl } } = response;
        return { payment_request_id: id, payment_request_url: longurl };
    }

    /**
     * @param {string} payment_request_id
     * @returns {boolean} Whether the payment succeeded or not
     */
    checkPaymentStatus(payment_request_id) {
        const request = {
            method: 'get',
            url: `${instamojo.BASE_URL}/payment-requests/${payment_request_id}`,
            headers: {
                'x-api-key': instamojo.API_KEY,
                'x-auth-token': instamojo.AUTH_TOKEN,
            }
        };
        const response = await axios(request);
        let { payment_request: { status } } = response;
        return status === "Completed";
    }

}

module.exports = {Instamojo}