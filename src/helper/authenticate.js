const passport = require('passport');
const jwt = require('jsonwebtoken');
const createError = require('http-errors');

function participantJWT(req, res, next) {
  return passport.authenticate('participant-jwt', (err, user, info) => {
    if (err) return next(createError(500, err)); // Error while checking : server error
    if (!user) return next(info); // Authentication failure : user error
    req.user = user;
    req.userType = 'participant';
    return next(); // Authentication successful
  })(req, res, next);
}
function adminJWT(req, res, next) {
  return passport.authenticate('admin-jwt', (err, user, info) => {
    if (err) return next(createError(500, err));
    if (!user) return next(info);
    req.user = user;
    req.userType = 'admin';
    return next();
  })(req, res, next);
}

function anyJWT(req, res, next) {
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    req.userType = 'public';
    return next();
  }
  const jwtToken = authHeader.split(' ')[1];
  const { type } = jwt.decode(jwtToken);
  if (type === 'super') {
    return adminJWT(req, res, next);
  }
  return participantJWT(req, res, next);
}

module.exports = {
  participantJWT,
  adminJWT,
  anyJWT,
};
