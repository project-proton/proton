const regexes = {
  objectName: /^(\w+['.,]? ?)*\w+$/,
  personName: /^([A-Za-z]+(\.? ?))*([A-Za-z]+\.?)$/,
  mongoHexId: /^[a-f0-9]{24}$/,
};

module.exports = regexes;
