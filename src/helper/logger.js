const fs = require('fs');
const path = require('path');
const file = path.resolve(__dirname, "../../logs");
module.exports = function log(err){
    fs.appendFileSync(file, JSON.stringify({log: err}, null, 2));
    fs.appendFileSync(file, "\n------------------------------------------------------------\n");
}