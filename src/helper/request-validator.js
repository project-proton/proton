const Joi = require('@hapi/joi');
const createError = require('http-errors');
const Extend = require('extend');

module.exports = function validate(schema, optionsRecieved) {
  const options = optionsRecieved || {};

  return function validateRequest(req, res, next) {
    const toValidate = {};
    ['params', 'body', 'query'].forEach((key) => {
      if (schema[key]) {
        toValidate[key] = req[key];
      }
    });

    function onValidationComplete(err, validated) {
      if (err) {
        const errmsgs = err.details.reduce((iter, e) => `${iter + e.message}.`, '');
        return next(createError(400, errmsgs));
      }

      // copy the validated data to the req object
      Extend(req, validated);

      return next();
    }

    return Joi.validate(toValidate, schema, options, onValidationComplete);
  };
};
