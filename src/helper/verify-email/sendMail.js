const sendgrid = require("@sendgrid/mail");
const makeMail = require('./makeMail');
const keys = require('../../config/keys').sendgrid;
const log = require('../logger');

sendgrid.setApiKey(keys.API_KEY);

async function sendVerificationMail(email_id, verification_link) {
    try {
        const mail = makeMail(verification_link);
        await sendgrid.send({
        from: 'Tathva \'19 <no-reply@tathva.org>', // sender address
        to: email_id, // list of receivers
        subject: mail.subject, // Subject line
        text: mail.plainText, // plain text body
        html: mail.html // html body
        });
    } catch(e) {
        log(e);
    }
}

module.exports = sendVerificationMail;