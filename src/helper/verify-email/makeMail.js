const fs = require('fs');
const path = require('path');
let plainText = fs.readFileSync(path.resolve(__dirname, 'text-mail-sample.txt'), 'utf-8');
let html = fs.readFileSync(path.resolve(__dirname, 'html-mail-sample.html'), 'utf-8');

function makeMail(verification_link){
    html = html.replace(/VERIFICATION_LINK/g, verification_link);
    plainText = plainText.replace(/VERIFICATION_LINK/g, verification_link);
    return { subject: "Confirm email | Tathva '19", html, plainText };
}

module.exports = makeMail;