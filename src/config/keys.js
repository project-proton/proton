const Joi = require('@hapi/joi');
require('dotenv').config();


const data = {
  NODE_ENV: process.env.NODE_ENV || 'development',
  PORT: process.env.PORT || 8000,
  PROTON_ROOT: process.env.PROTON_ROOT,
  mongodb: {
    MONGO_URI: process.env.MONGO_URI,
    MONGO_TEST_URI: process.env.MONGO_TEST_URI,
  },
  passport: {
    JWT_USER_SECRET: process.env.JWT_USER_SECRET,
    JWT_ADMIN_SECRET: process.env.JWT_ADMIN_SECRET,
    SALT_ROUNDS: Number(process.env.JWT_SALT_ROUNDS),
  },
  instamojo: {
    BASE_URL: process.env.IM_URL,
    API_KEY: process.env.IM_API_KEY,
    AUTH_TOKEN: process.env.IM_AUTH_TOKEN,
    REDIRECT_URL: process.env.IM_REDIRECT_URL,
    WEBHOOK_URL: process.env.IM_WEBHOOK_URL,
  },
  sendgrid: {
    API_KEY: process.env.SG_API_KEY
  }
};


// Defining validation schema
const constraints = {
  NODE_ENV: Joi.string().default('development').allow(['development', 'test', 'production']),
  PORT: Joi.number().port().default(8000),
  PROTON_ROOT: Joi.string().required(),
  passport: {
    JWT_USER_SECRET: Joi.string().required(),
    JWT_ADMIN_SECRET: Joi.string().required(),
    SALT_ROUNDS: Joi.number().required().positive().max(20),
  },
  instamojo: {
    BASE_URL: Joi.string().uri().required(),
    API_KEY: Joi.string().required(),
    AUTH_TOKEN: Joi.string().required(),
    REDIRECT_URL: Joi.string().uri().required(),
    WEBHOOK_URL: Joi.string().uri().required(),
  },
  sendgrid: {
    API_KEY: Joi.string().required(),
  }
};

if (process.env.NODE_ENV === 'test') {
  constraints.mongodb = Joi.object().keys({
    MONGO_TEST_URI: Joi.string().uri({ scheme: ['mongodb'] }).required(),
  }).unknown(true);
} else {
  constraints.mongodb = Joi.object().keys({
    MONGO_URI: Joi.string().uri({ scheme: ['mongodb'] }).required(),
  }).unknown(true);
}

const schema = Joi.object(constraints);

const { error, value } = Joi.validate(data, schema);

if (error) {
  console.error('Error validating config:', error.message);
  process.exit(1);
}

module.exports = value;
