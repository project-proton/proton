const db = require('../helper/mongoose');

module.exports = async () => {
  const conn = await db.connect();
  const collections = await conn.connection.db.collections();
  collections.forEach((collection) => {
    collection.deleteMany({});
  });
  await db.close();
};
