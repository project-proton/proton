const express = require('express');
const createError = require('http-errors');

// Routers
const indexRouter = require('./api/index/index-routes');
const adminRouter = require('./api/admins/admins-routes');
const collegesRouter = require('./api/colleges/colleges-routes');
const participantRouter = require('./api/participants/participants-routes');
const authRouter = require('./api/auth/auth-routes');
const eventsRouter = require('./api/events/events-routes');
const passesRouter = require('./api/passes/passes-routes');
const paymentRouter = require('./api/payment/payments-routes');

const { adminJWT } = require('./helper/authenticate');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Routing
app.use('/', indexRouter);
app.use('/admin', adminJWT, adminRouter);
app.use('/colleges', collegesRouter); // TODO Authenticate this endpoint
app.use('/auth', authRouter);
app.use('/participants', participantRouter);
app.use('/events', eventsRouter); // TODO Authenticate this endpoint
app.use('/passes', passesRouter); // TODO Authenticate this endpoint
app.use('/payments', paymentRouter);

// Responds with 404 error. Use all routes before this.
app.use((req, res, next) => {
  next(createError(404));
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  const { message, stack } = err;

  let status = res.statusCode === 200 ? err.statusCode || 500 : res.statusCode;
  if (['No auth token', 'invalid signature'].includes(err.message)) status = 401;

  res.status(status).json({
    message,
    status,
    stack: process.env.NODE_ENV === 'production' ? 'unavailable' : stack,
  });
});

module.exports = app;
