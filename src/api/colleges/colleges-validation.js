const Joi = require('@hapi/joi');

// Regex Patterns
const { objectName } = require('../../helper/regex-patterns');

// Validation schemas relating to college
const name = Joi.string().required().regex(objectName).min(6)
  .max(100);

const joiRequestSchemas = {
  addCollegeReq: {
    body: {
      name,
    },
  },
};

module.exports = joiRequestSchemas;
