const College = require('./colleges-model');
const Participant = require('../participants/participants-model');

async function getColleges(req, res, next) {
  try {
    const colleges = await Participant.distinct("college");
    res.json({ colleges });
  } catch (error) {
    next(error);
  }
}

async function addCollege(req, res, next) {
  const newCollege = req.body.name; // Name of new college
  try {
    const { _id } = await new College({ name: newCollege }).save();
    res.json({ message: `New college added with id ${_id}` });
  } catch (error) {
    if (error.code === 11000) res.status(400);
    next(error);
  }
}


module.exports = {
  getColleges,
  addCollege,
};

/*
  Could later seperate even more and make Service functions
  for listing and adding colleges

  Eg:
    CollegeService.listColleges could do the query
    the controller could be
    try{
      const colleges = await CollegeService.listUsers();
      res.json(colleges)
    } catch (error) {
      next(e)
    }

    CollegeService.createCollege could add colleges
    controller could be
    try{
      const college = await CollegeService.createCollege(name);
      res.json(college._id)
    } catch(error) {
      next(error)
    }
*/
