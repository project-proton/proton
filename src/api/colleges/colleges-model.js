const mongoose = require('mongoose');

const collegeSchema = mongoose.Schema({
  // name of the college in this column ( id is auto-generated )
  name: {
    type: String, unique: true, required: true,
  },
});

const collegeModel = mongoose.model('college', collegeSchema);

module.exports = collegeModel;
