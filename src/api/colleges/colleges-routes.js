const express = require('express');

const router = express.Router();
const { getColleges, addCollege } = require('./colleges-controller');
const joiSchemas = require('./colleges-validation');
const validator = require('../../helper/request-validator');
const { adminJWT } = require('../../helper/authenticate');

// get the list of colleges
router.get('/', getColleges);

// save a new college name
router.post('/',
  validator(joiSchemas.addCollegeReq),
  adminJWT,
  addCollege);


module.exports = router;
