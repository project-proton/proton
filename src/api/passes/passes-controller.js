/* // DOCS
//  This file has functions that are called at endpoints in the
    file ./passes-routes.js

//  All functions calls next() callback with the error when it occures.
    Some client side errors are responded right away.
*/
const createError = require('http-errors');

const Pass = require('./passes-model');

async function getPasses(req, res, next) {
  try {
    const result = await Pass.find({}).exec();
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
}

async function getPass(req, res, next) {
  const { id } = req.params;
  const desiredFields = {
    _id: 1,
    name: 1,
    category: 1,
    description: 1,
    meta: 1,
    booking_limit: 1,
    booked_count: 1,
    published: 1,
  };
  const query = { _id: id };
  if (req.userType !== 'admin') query.published = true;
  try {
    const doc = await Pass.findOne(query, desiredFields).exec();
    if (!doc) throw createError(404, 'No such pass');
    res.status(200).json(doc);
  } catch (error) {
    next(error);
  }
}

async function addPass(req, res, next) {
  const {
    name, category, description, meta, bookingLimit,
  } = req.body;
  const pass = {
    name, category, description, meta, booking_limit: bookingLimit,
  };
  try {
    const doc = await new Pass(pass).save();
    res.status(200).json({ message: 'New pass added', pass: doc });
  } catch (error) {
    next(error);
  }
}

// This function assumes all fields are given in the request.
// Undefined fields will set corresponding database field to null.
async function putPass(req, res, next) {
  const { id } = req.params;
  const edits = {};
  edits.name = req.body.name;
  edits.category = req.body.category;
  edits.description = req.body.description;
  edits.meta = req.body.meta;
  edits.booking_limit = req.body.booking_limit;
  try {
    const doc = await Pass.findByIdAndUpdate(id, edits, { new: true }).exec();
    if (!doc) throw createError(404, 'No such pass');
    res.status(200).json({ message: 'Updated', pass: doc });
  } catch (error) {
    next(error);
  }
}


const Participant = require('../participants/participants-model');

async function getPassRegistration(req, res, next) {
  const { id } = req.params;
  try {
    const participants = await Participant.find({ passes: id }, 'authID name');
    res.json({ pass: id, count: participants.length, participants });
  } catch (error) {
    next(error);
  }
}

module.exports = {
  getPasses,
  getPass,
  addPass,
  putPass,
  getPassRegistration,
};
