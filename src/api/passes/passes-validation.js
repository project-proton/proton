const Joi = require('@hapi/joi');

const { objectName, mongoHexId } = require('../../helper/regex-patterns');

// Joi validation schemas relating to pass
const _id = Joi.string().regex(mongoHexId);
const name = Joi.string().required().regex(objectName).min(6)
  .max(100);
const description = Joi.string().optional().min(10).max(300)
  .allow(null);

// Joi schemas wrapped up to suit requests
const joiRequestSchemas = {
  addPassReq: {
    body: {
      name,
      description,
    },
  },
  getPassesReq: {
  },
  putPassReq: {
    body: {
      _id,
      name,
      description,
    },
  },
};

module.exports = joiRequestSchemas;
