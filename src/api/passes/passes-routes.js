/* DOCS
//  Supported requests
//  get '/' - shows a list of all pass's name, type, and id.
//  put '/' - creates or updates an pass depending on weather
              the id supplied in body is null or not.
//  get '/:id' - shows all details of pass specified by id.
//  post '/publish' - publishes an pass whose _id is given in body
              if necessary fields are complete.
              Necessarry fields to be filled are:
                name
                pnt_lim - Upper limit on participants
//  post '/unpublish' - Unpublishes an pass whose _id is given in body
*/

const router = require('express').Router();
const validator = require('../../helper/request-validator');
const joiSchemas = require('./passes-validation');
const { adminJWT, anyJWT } = require('../../helper/authenticate');

const {
  getPasses, addPass, putPass,
  getPassRegistration,
} = require('./passes-controller');

// Get all published passes.
router.get('/',
  validator(joiSchemas.getPassesReq),
  anyJWT,
  getPasses);
// [Admin Route] Add passes.
router.post('/',
  validator(joiSchemas.addPassReq),
  adminJWT,
  addPass);
// [Admin Route] Modifies pass.
router.put('/:id',
  validator(joiSchemas.putPassReq),
  adminJWT,
  putPass);

router.get('/:id/participants',
  validator(joiSchemas.getPassReq),
  adminJWT,
  getPassRegistration);

module.exports = router;
