/* // DOCS
//  This file has functions that are called at endpoints in the
    file ./events-routes.js

//  All functions calls next() callback with the error when it occures.
    Some client side errors are responded right away.
*/
const createError = require('http-errors');

const Event = require('./events-model');

async function getEvents(req, res, next) {
  const query = req.query.category ? { category: req.query.category } : {};
  if (req.userType !== 'admin') query.published = true;
  try {
    const result = await Event.find(query, {
      _id: 1, name: 1, category: 1, price: 1, description: 1
    }).exec();
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
}

async function getEvent(req, res, next) {
  const { id } = req.params;
  const desiredFields = {
    _id: 1,
    name: 1,
    category: 1,
    description: 1,
    price: 1,
    meta: 1,
    booking_limit: 1,
    booked_count: 1,
    published: 1,
  };
  const query = { _id: id };
  if (req.userType !== 'admin') query.published = true;
  try {
    const event = await Event.findOne(query, desiredFields).populate('held', 'expiry').exec();
    if (!event) throw createError(404, 'No such event');
    event._doc.stock = event.stock;
    event._doc.pending_payment = event.pending;
    if (req.query.held !== 'true') {
      delete event._doc.held;
    }
    res.status(200).json(event);
  } catch (error) {
    next(error);
  }
}

async function addEvent(req, res, next) {
  const {
    // eslint-disable-next-line camelcase
    name, category, description, meta, booking_limit, price,
  } = req.body;
  const event = {
    name, category, description, meta, price, booking_limit,
  };
  try {
    const doc = await new Event(event).save();
    res.status(200).json({ message: 'New event added', event: doc });
  } catch (error) {
    next(error);
  }
}

// This function assumes all fields are given in the request.
// Undefined fields will set corresponding database field to null.
async function putEvent(req, res, next) {
  const { id } = req.params;
  const edits = {};
  edits.name = req.body.name;
  edits.category = req.body.category;
  edits.description = req.body.description;
  edits.meta = req.body.meta;
  edits.booking_limit = req.body.booking_limit;
  edits.price = req.body.price;
  try {
    const doc = await Event.findByIdAndUpdate(id, edits, { new: true }).exec();
    if (!doc) throw createError(404, 'No such event');
    res.status(200).json({ message: 'Updated', event: doc });
  } catch (error) {
    next(error);
  }
}


async function changePublicity(req, res, next) {
  const eventId = req.body._id;
  const { published } = req.body;

  // Setting publicity to false directly
  if (published === false) {
    try {
      const doc = await Event.findByIdAndUpdate(eventId, { published }).exec();
      if (!doc) throw createError(404, 'No such event');
      res.status(200).json({ message: 'Unpublished' });
    } catch (error) {
      next(error);
    }
  } else {
  // Setting publicity to true after checking required fields
    try {
      const doc = await Event.findById(eventId).exec();
      if (!doc) throw createError(404, 'No such event');
      if (doc.booking_limit != null && doc.price) {
        await Event.findByIdAndUpdate(eventId, { published }).exec();
        res.status(200).json({ message: 'Published' });
      } else throw Error('Incomplete Event');
    } catch (error) {
      if (error.message === 'Incomplete Event') res.status(417);
      next(error);
    }
  }
}

const Participant = require('../participants/participants-model');

async function getEventParticipation(req, res, next) {
  const { id } = req.params;
  try {
    const participants = await Participant.find({ events: id }, 'authID name');
    res.json({ event: id, count: participants.length, participants });
  } catch (error) {
    next(error);
  }
}

// // Test Controllers: TODO : to be removed in production
// async function reduceStock(req, res, next) {
//   const { id } = req.params;
//   try {
//     const changed = await Event.changeStock(id, -1);
//     res.json({ booked_count: changed.booked_count });
//   } catch (error) {
//     next(error);
//   }
// }

module.exports = {
  getEvents,
  getEvent,
  addEvent,
  putEvent,
  changePublicity,
  getEventParticipation,

  // reduceStock,
};
