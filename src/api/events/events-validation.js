const Joi = require('@hapi/joi');

const { objectName, mongoHexId } = require('../../helper/regex-patterns');

// Joi validation schemas relating to event
const _id = Joi.string().regex(mongoHexId);
const name = Joi.string().required().regex(objectName).min(6)
  .max(100);
const category = Joi.string().optional().lowercase().regex(/^[a-z:_]+$/, { name: 'numbers'})
  .min(3)
  .max(20)
  .allow(null);
const description = Joi.string().optional().min(10).max(300)
  .allow(null);
const meta = Joi.string().optional().max(300).allow(null);
const bookingLimit = Joi.number().optional().integer().positive()
  .max(1000)
  .allow(null);
const price = Joi.number().required();
const published = Joi.boolean().required();
const forbidden = Joi.any().forbidden();

// Joi schemas wrapped up to suit requests
const joiRequestSchemas = {
  addEventReq: {
    body: {
      name,
      category,
      description,
      meta,
      price,
      booking_limit: bookingLimit,
      booked_count: forbidden,
      published: forbidden,
    },
  },
  getEventReq: {
    params: {
      id: _id,
    },
  },
  getEventsReq: {
    query: {
      category,
    },
  },
  putEventReq: {
    body: {
      _id,
      name,
      category,
      description,
      meta,
      price,
      booking_limit: bookingLimit,
      booked_count: forbidden,
      published: forbidden,
    },
  },
  changePublicityReq: {
    body: {
      _id,
      published,
    },
  },
};

module.exports = joiRequestSchemas;
