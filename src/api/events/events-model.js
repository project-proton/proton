/* eslint-disable func-names */
const mongoose = require('mongoose');
const moment = require('moment');
/*
//  The schema has marked all fields apart from event name
    as not required. It is to facilitate progressive filling of
    data.

//  Explanation
    name: Event name
    type: Eg. Workshop, lecture etc. TODO We need to allow admins
      to access a list of types which they can expand
    desc: Describing Event
    meta: Intented to store extra data as json string. It is to be manipulated
      by front-end developers if needed.
    pnt_lim: Upper limit on participants.
    pnt_cnt: Number of people registered.
    published: Boolean denoting weather this event is visible to public

//  The field published is not supposed to be edited like other fields.
    Instead, a publish function should check if necessary fields are
    completed and then set this field to true.

//  Participant Count is to be manipulated only by the agent that
    do registrations.

//  NOTE: Unique id for events will be autogenerated and
    accessible in _id field which is not shown here.

*/

const eventSchemaOptions = {
  // toJSON: {
  //   virtuals: true,
  //   // getters: true,
  // },
  // toObject: {
  //   virtuals: true,
  //   // getters: true,
  // },
};
const eventSchema = mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true,
  },
  category: {
    type: String,
    unique: false,
    required: false,
  },
  description: {
    type: String,
    unique: false,
    required: false,
  },
  /* For fields that are specific to a category of events. */
  meta: {
    type: String,
    unique: false,
    required: false,
  },
  price: {
    type: Number,
    required: false,
  },
  booking_limit: {
    type: Number,
    unique: false,
    required: false,
  },
  booked_count: {
    type: Number,
    unique: false,
    required: true,
    default: 0,
  },
  held: [{
    type: mongoose.ObjectId,
    ref: 'payment',
  }],
  published: {
    type: Boolean,
    unique: false,
    required: true,
    default: false,
  },
}, eventSchemaOptions);

// async function changeStock(id, change) {
//   try {
//     const event = await this.findById(id).exec();
//     if (event.booking_limit <= event.booked_count) throw new Error('booking limit reached');
//     console.log('reducing count');
//     return this.findByIdAndUpdate(id, { $inc: { booked_count: -1 * change } }, { new: true });
//   } catch (error) {
//     error.statusCode = 422;
//     console.log('Error in changeStock');
//     throw error;
//   }
// }

// eventSchema.statics.changeStock = changeStock;
eventSchema.virtual('pending').get(function () {
  const { held } = this;
  const pendingPayments = held.filter(payment => moment().isBefore(payment.expiry));
  return pendingPayments.length;
});
eventSchema.virtual('stock').get(function () {
  return this.booking_limit - this.booked_count - this.pending;
});
const eventModel = mongoose.model('event', eventSchema);

module.exports = eventModel;
