/* DOCS
//  Supported requests
//  get '/' - shows a list of all event's name, type, and id.
//  put '/' - creates or updates an event depending on weather
              the id supplied in body is null or not.
//  get '/:id' - shows all details of event specified by id.
//  post '/publish' - publishes an event whose _id is given in body
              if necessary fields are complete.
              Necessarry fields to be filled are:
                name
                pnt_lim - Upper limit on participants
//  post '/unpublish' - Unpublishes an event whose _id is given in body
*/

const router = require('express').Router();
const validator = require('../../helper/request-validator');
const joiSchemas = require('./events-validation');
const { adminJWT, anyJWT } = require('../../helper/authenticate');

const {
  getEvents, getEvent, addEvent, putEvent,
  changePublicity, getEventParticipation,
} = require('./events-controller');

// Get all published events.
router.get('/',
  validator(joiSchemas.getEventsReq),
  anyJWT,
  getEvents);
// [Admin Route] Add events.
router.post('/',
  validator(joiSchemas.addEventReq),
  adminJWT,
  addEvent);
// Get single event details. only gets published events.
router.get('/:id',
  validator(joiSchemas.getEventReq),
  anyJWT,
  getEvent);
// [Admin Route] Modifies event.
router.put('/:id',
  validator(joiSchemas.putEventReq),
  adminJWT,
  putEvent);
// [Admin Route] Publish/unpublish events
router.post('/publish',
  validator(joiSchemas.changePublicityReq),
  adminJWT,
  changePublicity);


router.get('/:id/participants',
  validator(joiSchemas.getEventReq),
  adminJWT,
  getEventParticipation);

// Test Routes : TODO : to be removed in production
// const { reduceStock } = require('./events-controller');

// router.get('/:id/reduce', reduceStock);
module.exports = router;
