const mongoose = require('mongoose');
const moment = require('moment');

const { Schema } = mongoose;

const paymentSchema = new Schema({
  participant: { type: mongoose.ObjectId, ref: 'participant' },
  payment_request_id: { type: String, required: true },
  expiry: {
    type: Date,
    default: moment().add(5, 'minutes').toISOString(),
  },
});

const paymentModel = mongoose.model('payment', paymentSchema);

module.exports = paymentModel;
