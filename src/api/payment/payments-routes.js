const router = require('express').Router();
const { generateLink, verifyPayment } = require('./payments-controllers');
const { participantJWT } = require('../../helper/authenticate');

async function cartCheckout(req, res, next) {
  const { events } = req.body;
  const { user } = req;
  try {
    const prices = await generateLink(user, events);
    res.json(prices);
  } catch (error) {
    next(error);
  }
}

router.post('/checkout', participantJWT, cartCheckout);
router.get('/verify', verifyPayment);

const { findEventsByPayment } = require('./payments-controllers');

router.get('/test/:id', async (req, res) => {
  const { id: paymentid } = req.params;
  const result = await findEventsByPayment(null, paymentid);
  res.json(result);
});
module.exports = router;
