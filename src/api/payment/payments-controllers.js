const path = require('path');
const mongoose = require('mongoose');
const axios = require('axios');
const moment = require('moment');
const Event = require('../events/events-model');
const Participant = require('../participants/participants-model');
const Payment = require('./payments-model');
const { instamojo } = require('../../config/keys');

const paymentLink = instamojo.BASE_URL;

/**
 * Gets total price for a list of events
 * @param {Array} events A list of event ids to generate total for.
 * @returns {object} containing a list of events and total price.
 */
async function getPrices(events) {
  const eventids = events.map(event => new mongoose.Types.ObjectId(event));
  const eventprices = await Event.find({}, '_id price')
    .where('_id')
    .in(eventids);
  const total = eventprices.reduce(
    (totalprice, event) => totalprice + event.price,
    0,
  );
  return {
    events: eventprices,
    total,
  };
}
/**
 * Verifies availability of events for registration
 * @param {Array} events List of events to check stock of.
 * @returns {bool} true if all events are available, false otherwise
 */
async function verifyStock(events) {
  let available = true;
  const eventids = events.map(event => new mongoose.Types.ObjectId(event));
  const bookings = await Event.find({}, '_id booking_limit booked_count')
    .where('_id')
    .in(eventids);
  const stocks = bookings.map(
    event => event.booking_limit - event.booked_count,
  );
  stocks.forEach((stock) => {
    if (stock < 1) available = false;
  });
  return available;
}
// async function reduceStock(events) {
//   const reducePromises = [];
//   events.forEach((event) => {
//     reducePromises.push(Event.changeStock(event, -1));
//   });
//   try {
//     return await Promise.all(reducePromises);
//   } catch (error) {
//     throw error;
//   }
// }
// async function increaseStock(events) {
//   const reducePromises = [];
//   events.forEach((event) => {
//     reducePromises.push(Event.changeStock(event, 1));
//   });
//   try {
//     await Promise.all(reducePromises);
//   } catch (error) {
//     throw error;
//   }
// }

/**
 * Generate payment link for a list of events using the instamojo API
 * @param {String} user object with participant info
 * @param {Array} events A list of events to generate link for
 */
async function generateLink(user, events) {
  const offsetTime = moment().add(3, 'minutes').toISOString();
  const eventids = events.map(event => new mongoose.Types.ObjectId(event));

  try {
    const eventprices = await getPrices(events);
    const stockAvailable = await verifyStock(events);
    if (!stockAvailable) throw new Error('nostock');
    const { total } = eventprices;
    const response = await axios({
      method: 'post',
      url: `${paymentLink}/payment-requests/`,
      data: {
        amount: total,
        purpose: `Cart: ${user.name}`,
        buyer_name: user.name,
        email: user.email,
        phone: user.mobile,
        redirect_url: 'https://edvinbasil.com/redirect',
        webhook: 'https://inspect.edvinbasil.com',
        allow_repeated_payments: 'false',
        // send_email: 'true',
        // send_sms: 'true',
        expires_at: offsetTime,
      },

      headers: {
        'x-api-key': instamojo.API_KEY,
        'x-auth-token': instamojo.AUTH_TOKEN,
        'content-type': 'application/json',
      },
    });
    // await reduceStock(events);
    const paymentRecord = {
      participant: user._id,
      payment_request_id: response.data.payment_request.id,
    };
    const { _id } = await new Payment(paymentRecord).save();
    await Event.updateMany({}, { $push: { held: _id } })
      .where('_id')
      .in(eventids);
    const { data } = response;
    // TODO: check if successful only then reduce stock
    // TODO: If payment total < 9 && > 0; send proper error message [cant process]
    return { ...data, ...eventprices };
  } catch (err) {
    if (err.message === 'nostock') {
      err.statusCode = 422;
      err.message = 'Stocks unavailable';
    }
    // console.log(err.message);
    return { response: err.response.data };
    // throw err;
  }
}

/**
 * Get a list of events that a payment request corresponds to.
 * either pid or paymentid is required, not both
 * @param {string} pid id of payment record from payments collection
 * @param {string} paymentid Instamojo payment id
 * @returns {Array} List of events corresponding to payemnt request
 */
async function findEventsByPayment(pid, paymentid) {
  let _id = pid;
  if (!_id) _id = await Payment.findOne({ payment_request_id: paymentid }).exec();
  return Event.find({ held: _id }).exec();
}


async function verifyPayment(req, res, next) {
  try {
    const { payment_id, payment_status, payment_request_id } = req.query;
    console.log(`${paymentLink}/payments/${payment_request_id}`);
    const response = await axios({
      method: 'get',
      url: `${paymentLink}/payments/${payment_id}`,
      headers: {
        'x-api-key': instamojo.API_KEY,
        'x-auth-token': instamojo.AUTH_TOKEN,
        'content-type': 'application/json',
      },
    });
    const { data } = response;
    const pr_id = path.basename(data.payment.payment_request);
    if (pr_id === payment_request_id && data.success) {
      const payment_record = await Payment.findOne({ payment_request_id });
      console.log(payment_record);
      const participant = payment_record.participant;
      const events = await findEventsByPayment('', payment_request_id);
      await Participant.findByIdAndUpdate(participant, { $push: { events } });
      return res.json(events);
    }
    return res.json({ msg: 'payment failed' });
  } catch (err) {
    return next(err);
  }
}
// https://edvinbasil.com/redirect
// ?payment_id=MOJO9809705A96413204&payment_status=Credit
// &payment_request_id=29cbfd24f49d449da135f071f5075b57


// ?payment_id=MOJO9627P05A66016359
// &payment_status=Credit
// &payment_request_id=8bd57698405d48c19ee1b700fb5ceb83

module.exports = {
  getPrices,
  generateLink,
  // reduceStock,
  // increaseStock,
  verifyStock,
  findEventsByPayment,
  verifyPayment,
};
