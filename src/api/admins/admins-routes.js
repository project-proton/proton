const router = require('express').Router();

router.get('/', (req, res) => res.json({ message: 'Authorised endpoint' }));

module.exports = router;
