/*
// This file would contain the mongose schema and model
// It will export this model so that other files can access it.
// Something like

const UserSchema = new Schema({
    name: String
    //etc
});

module.exports = mongoose.model('user', UserSchema);

*/

const mongoose = require('mongoose');

const { Schema } = mongoose;

const adminSchema = new Schema({
  username: {
    type: String, unique: true, required: true,
  },
  hashword: {
    type: String, unique: false, required: true,
  },
});

const adminModel = mongoose.model('authAdmin', adminSchema);

module.exports = adminModel;
