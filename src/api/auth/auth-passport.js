// Modules to define passport strategies
const passport = require('passport');
const bcrypt = require('bcrypt');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require('passport-jwt');
const createError = require('http-errors');

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const { JWT_ADMIN_SECRET, JWT_USER_SECRET } = require('../../config/keys').passport;

// Mongoose Models
const participantModel = require('../participants/participants-model');
const adminModel = require('../admins/admins-model');

// ------------DEFINING PASSPORT STRATEGIES------------------
passport.use('participant-local', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
},
(username, password, cb) => participantModel.findOne({ authID: username })
  .then((user) => {
    if (!user) return cb(null, false, createError(404, 'No such participant'));
    if (user.authClass === 'local') {
      return bcrypt.compare(password, user.hashword, (err, same) => {
        if (err) {
          return cb(err, false, createError(500, 'Checking failed'));
        }
        if (same) {
          return cb(null, user, 'Auth Successful');
        }
        return cb(null, false, createError(401, 'Invalid credential'));
      });
    }
    return cb(null, false, createError(null, 404, 'No such id for local authentication'));
  })
  .catch(err => cb(err, false, createError(500, 'Checking failed')))));

passport.use('admin-local', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
},
(username, password, cb) => adminModel.findOne({ username })
  .then((user) => {
    if (!user) return cb(null, false, createError(404, 'No such admin'));
    return bcrypt.compare(password, user.hashword, (err, same) => {
      if (err) {
        return cb(err, false, createError(500, 'Ckecking failed'));
      }
      if (same) {
        return cb(null, user, 'Auth Successful');
      }
      return cb(null, false, createError(401, 'Invalid credential'));
    });
  })
  .catch(err => cb(err, false, createError(500, 'Ckecking failed')))));

passport.use('participant-jwt', new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: JWT_USER_SECRET,
}, (jwtPayload, cb) => participantModel.findById(jwtPayload._id, (err, dbEntry) => {
  if (err) return cb(err, false, createError(500, 'Checking failed'));
  if (!dbEntry) return cb(null, false, createError(404, 'No such participant'));
  return cb(null, dbEntry, 'Auth Successful'); // Will probably change what is returned when participant has more attributes
})));

passport.use('admin-jwt', new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: JWT_ADMIN_SECRET,
},
(jwtPayload, cb) => adminModel.findById(jwtPayload._id, (err, dbEntry) => {
  if (err) return cb(err, false, createError(500, 'Checking failed'));
  if (!dbEntry) return cb(null, false, createError(404, 'No such admin'));
  return cb(null, dbEntry, 'Auth Successful');
})));
// ------------END OF DEFINING PASSPORT STRATEGIES-----------
