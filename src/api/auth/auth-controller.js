const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const createError = require('http-errors');
const passport = require('passport');
const sendVerificationMail = require('../../helper/verify-email/sendMail');
const { PROTON_ROOT } = require('../../config/keys');
const { JWT_ADMIN_SECRET, JWT_USER_SECRET, SALT_ROUNDS } = require('../../config/keys').passport;
const ParticipantModel = require('../participants/participants-model');
const AdminModel = require('../admins/admins-model');
const { NODE_ENV } = require('../../config/keys');

function filterObject(original, allowed) {
  return Object.keys(original)
    .filter(key => allowed.includes(key))
    .reduce((obj, key) => ({
      ...obj,
      [key]: original[key],
    }), {});
}

async function verifyEmailByCode(code){
  if(code.length != 30) return false;
  const authID = code.slice(0, 24);
  const secret = code.slice(24, 30);
  const doc = await ParticipantModel.findById(authID);
  if(!doc) return false;
  else if(doc.email_secret != secret) return false;
  else {
    doc.email_verified = true;
    try { 
      await doc.save();
    } catch(e) {
      return false;
    }
    return true;
  }
}

function getParticipantToken(req, res) {
  // Reaches here only after successful local auth
  const { _id, authID, email_verified } = req.user;
  if(NODE_ENV != "test" && !email_verified) 
    return res.status(403).json({message: "Email verification pending"});
  const payLoad = { _id };

  // TODO Set jwt options like exp
  const token = jwt.sign(payLoad, JWT_USER_SECRET);
  return res.json({ username: authID, token });
}

function getAdminToken(req, res) {
  // Reaches here only after local admin auth
  const { _id, username } = req.user;
  const payLoad = { type: 'super', _id };
  // TODO Set jwt options like exp here too
  const token = jwt.sign(payLoad, JWT_ADMIN_SECRET);
  return res.json({ username, token });
}

async function registerParticipant(req, res, next) {
  const { username, password } = req.body;
  const editableFields = "name college email mobile";
  const allowed = editableFields.split(' ');
  const details = filterObject(req.body, allowed);
  try {
    let participantId, pEmail, pEmailSecret;
    const hash = await bcrypt.hash(password, SALT_ROUNDS);
    const participant = new ParticipantModel({
      authClass: 'local',
      authID: username,
      hashword: hash,
      ...details,
    });
    try {
      const { _id, email, email_secret } = await participant.save();
      participantId = _id;
      pEmail = email;
      pEmailSecret = email_secret;
    } catch (error) {
      if (error.code === 11000 && error.message.indexOf('short_id')) {
        const { _id, email, email_secret } = await participant.save();
        participantId = _id;
        pEmail = email;
        pEmailSecret = email_secret;
      }
    } 
    
    if(!participantId) return res.status(500).json({message: "Could not create participant"});
    const verification_link = PROTON_ROOT+"/auth/verify-email?code="+participantId+pEmailSecret;
    sendVerificationMail(pEmail, verification_link);
    res.json({ message: `Participant with id ${participantId} has been registered. An email asking confirmation has been sent to ${pEmail}. Please confirm your mail before logging in.` });
  } catch(e) {
    next(e);
  }
}

async function verifyEmail(req, res, next){
  const code = req.query.code;
  try {
    if(await verifyEmailByCode(code)){
      res.json({message: "Email verified"});
    }
    else res.status(400).json({message: "Could not verify link"});
  } catch(e) {
    next(e);
  }
}

async function sendVerificationLink(req, res, next){
  const { username, password } = req.body;
  try {
    const participant = await ParticipantModel.findOne({ authClass: "local", authID: username });
    if(!participant) res.status(400).json({ message: "No such participant" });
    else if(await bcrypt.compare(password, participant.hashword)){
      const verification_link = PROTON_ROOT+"/auth/verify-email?code="+participant._id+participant.email_secret;
      sendVerificationMail(participant.email, verification_link);
      return res.json({message: "Email has been send"})
    } else {
      return res.status(401).json({message: "Wrong password"});
    }
  } catch(e) {
    next(e);
  }
}

async function registerAdmin(req, res, next) {
  const { username, password } = req.body;
  try {
    const hash = await bcrypt.hash(password, SALT_ROUNDS);

    const newAdmin = new AdminModel({ username, hashword: hash });
    const { _id } = await newAdmin.save();
    res.json({ message: `Added admin with id: ${_id}` });
  } catch (error) {
    if (error.code === 11000) res.status(400);
    next(error);
  }
}

function participantLocal(req, res, next) {
  return passport.authenticate('participant-local', (err, user, info) => {
    if (err) return next(createError(500, err)); // Error while checking : server error
    if (!user) return next(info); // Authentication failure : user error
    req.user = user;
    return next(); // Authentication successful
  })(req, res, next);
}
function adminLocal(req, res, next) {
  return passport.authenticate('admin-local', (err, user, info) => {
    if (err) return next(createError(500, err));
    if (!user) return next(info);
    req.user = user;
    return next();
  })(req, res, next);
}

module.exports = {
  getParticipantToken,
  registerParticipant,
  getAdminToken,
  registerAdmin,
  adminLocal,
  participantLocal,
  sendVerificationLink,
  verifyEmail,
};
