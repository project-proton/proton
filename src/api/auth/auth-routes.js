const router = require('express').Router();
const { NODE_ENV } = require('../../config/keys');
const joiSchemas = require('./auth-validation');
const validator = require('../../helper/request-validator');

const {
  getParticipantToken,
  registerParticipant,
  getAdminToken,
  registerAdmin,
  participantLocal,
  adminLocal,
  verifyEmail,
  sendVerificationLink,
} = require('./auth-controller');

require('./auth-passport');

const validatorMiddleware = validator(joiSchemas.loginForm);

// ------------MAKING AUTHENTICATION ENDPOINTS---------------
router.post('/participant/get-token',
  validatorMiddleware,
  participantLocal,
  getParticipantToken);

router.post('/participant/register',
  validator(joiSchemas.participantRegister),
  registerParticipant);

router.post('/admin/get-token',
  validatorMiddleware,
  adminLocal,
  getAdminToken);

router.get('/verify-email', verifyEmail);
router.post('/send-verification-link', sendVerificationLink);

// Use the following commented signup endpoint only to manually sign up an admin
// A form with username and password fields is enough to sign in
// Added safety check to never enable on production by default

if (NODE_ENV !== 'production') {
  router.post('/admin/register',
    validatorMiddleware,
    registerAdmin);
}

// -----------END OF MAKING AUTHENTICATION ENDPOINTS---------

module.exports = router;
