const Joi = require('@hapi/joi');

// Joi schemas relating to auth
const username = Joi.alternatives()
  .when(Joi.string().regex(/@/),
    {
      then: Joi.string().email(),
      otherwise: Joi.string().alphanum().min(6).max(30),
    }).required();

const password = Joi.string().required().min(6).max(30);
const email = Joi.string().email().required();

const joiRequestSchemas = {
  loginForm: {
    body: {
      username,
      password,
    },
  },
  participantRegister: {
    body: {
      username,
      password,
      email,
      name: Joi.string(),
      college: Joi.string(),
      mobile: Joi.string().regex(/\d{10}/),
    }
  }
};

module.exports = joiRequestSchemas;
