const createError = require('http-errors');
const Participant = require('./participants-model');

const publicfields = '_id authID short_id name college email mobile'; // TODO: remove events in production
const editableFileds = 'name college email mobile';

function filterObject(original, allowed) {
  return Object.keys(original)
    .filter(key => allowed.includes(key))
    .reduce((obj, key) => ({
      ...obj,
      [key]: original[key],
    }), {});
}

async function getSelf(req, res, next) {
  try {
    const participant = await Participant
      .findById(req.user._id, publicfields).populate('events', 'name category').exec();
    res.json({ participant });
  } catch (error) {
    next(error);
  }
}
async function editSelf(req, res, next) {
  const allowed = publicfields.split(' ');
  const details = filterObject(req.body, allowed);
  try {
    const participant = await Participant.findByIdAndUpdate(req.user._id, details, { new: true });
    res.json({ participant });
  } catch (error) {
    next(error);
  }
  res.end();
}
async function deleteSelf(req, res, next) {
  const { _id } = req.user;
  try {
    const participant = await Participant.findById(_id);
    if (participant.events.length > 0) throw createError(409, 'Participant has events registered');
    const deleted = await Participant.findOneAndDelete({ _id: req.user._id });
    res.json({ id: deleted._id });
  } catch (error) {
    next(error);
  }
}

// Admin Controllers
async function getAllParticipants(req, res, next) {
  try {
    const participants = await Participant
      .find({}, publicfields).populate('events', 'name category').exec();
    res.json({ participants });
  } catch (error) {
    next(error);
  }
}
async function getParticipant(req, res, next) {
  try {
    const { id } = req.params;
    const query = id.length === 6 ? { short_id: id } : { _id: id };
    const participant = await Participant
      .findOne(query, publicfields).populate('events', 'name category').exec();
    if (!participant) throw createError(404, 'No such participant');
    res.json({ participant });
  } catch (error) {
    next(error);
  }
}
async function editParticipant(req, res, next) {
  const allowed = editableFileds.split(' ');
  const details = filterObject(req.body, allowed);
  try {
    const participant = await Participant.findByIdAndUpdate(req.params.id, details, { new: true });
    if (!participant) throw createError(404, 'No such participant');
    res.json({ participant });
  } catch (error) {
    next(error);
  }
}
async function deleteParticipant(req, res, next) {
  const { id } = req.params;
  try {
    const participant = await Participant.findById(id);
    if (!participant) throw createError(404, 'No such participant');
    if (participant.events.length > 0) throw createError(409, 'Participant has events registered');
    const deleted = await Participant.findOneAndDelete({ _id: id });
    res.json({ id: deleted._id });
  } catch (error) {
    next(error);
  }
}

module.exports = {
  getSelf,
  editSelf,
  deleteSelf,

  getAllParticipants,
  getParticipant,
  editParticipant,
  deleteParticipant,
};
