/*
// This is just sample code of what could be in this file

const router = require('express').Router();
const User = require('./users-model');


//    User interaction with the database through the mongoose model
      that is imported from users-model.js
//    So we could use functions like
//    - User.save()
//    - User.findOne()
//    - etc


router.get('/', (req, res, next) => {
 // Handle the route
});

module.exports = router;
*/

const router = require('express').Router();
const { participantJWT, adminJWT } = require('../../helper/authenticate');

// Requiring Controllers
const {
  getSelf,
  editSelf,
  deleteSelf,

  getAllParticipants,
  getParticipant,
  editParticipant,
  deleteParticipant,
} = require('./participants-controller');

// Participant Routes
router.get('/self', participantJWT, getSelf);
router.put('/self', participantJWT, editSelf);
router.delete('/self', participantJWT, deleteSelf);

// Admin Routes

router.get('/', adminJWT, getAllParticipants);
router.get('/:id', adminJWT, getParticipant);
router.put('/:id', adminJWT, editParticipant);
router.delete('/:id', adminJWT, deleteParticipant);

module.exports = router;
