/*
// This file would contain the mongose schema and model
// It will export this model so that other files can access it.
// Something like

const UserSchema = new Schema({
    name: String
    //etc
});

module.exports = mongoose.model('user', UserSchema);

*/
const mongoose = require('mongoose');
const generate = require('nanoid/generate');

const shortchars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const hexchars = '0123456789abcdef'

const { Schema } = mongoose;

const participantSchema = new Schema({
  authClass: {
    type: String, unique: false, required: true,
  },
  authID: {
    type: String, unique: true, required: true,
  },
  email_secret: {
    type: String, default: () => generate(hexchars, 6),
  },
  email_verified: {
    type: Boolean, default: false
  },
  hashword: {
    type: String, unique: false, required: false,
  },
  short_id: {
    type: String, unique: true, default: () => generate(shortchars, 6),
  },
  name: {
    type: String, unique: false, required: false,
  },
  college_id: {
    type: String, unique: false, required: false,
  },
  college: String,
  email: {
    type: String, unique: false, required: false,
  },
  mobile: {
    type: String, unique: false, required: false,
  },
  events: [{ type: mongoose.ObjectId, ref: 'event' }],
  passes: [{ type: mongoose.ObjectId, ref: 'pass' }],
  referral_code: {
    type: String, unique: false, required: false,
  },
});

const participantModel = mongoose.model('authParticipant', participantSchema);

module.exports = participantModel;
