# Proton Backend Server

This directory contains the source files for the express backend server used to build proton server

## Running locally

Clone the repository and cd into the src directory
```
git clone https://gitlab.com/project-proton/proton
cd proton
```

Install the required npm modules

    npm install

Rename `.env.sample` to `.env`
Configure proton using the config keys in `.env`

Running the server for local development

    npm run dev

this will run nodemon which will refresh the server on changes

The server will be available on port 8000 by default unless otherwise specified and can be accessed at http://localhost:8000


### Configuring the database

The database URI is provided can be provided in a .env file inside the src directory. 
Create a `.env` file in this directory.
Specify the mongo db uri in the format
```
MONGO_URI=mongodb://<db_user>:<db_password>@<db_host>:<port>/<authentication_db>
```
incase of a local installation of mongo, this is generally
```
MONGO_URI=mongodb://localhost:27017
```


## Contributions

TBA


